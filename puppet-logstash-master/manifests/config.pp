# This class manages configuration directories for Logstash.
#
# @example Include this class to ensure its resources are available.
#   include logstash::config
#
# @author https://github.com/elastic/puppet-logstash/graphs/contributors
#
class profile::logstash::config {
  require profile::logstash::package

  File {
    owner => 'root',
    group => 'root',
  }



$ensure        = lookup('profile::logstash::ensure', String, deep)
$config_dir    = lookup('profile::logstash::config_dir', String, deep)
$purge_config  = lookup('profile::logstash::purge_config', String, deep)


  if($ensure == 'present') {
    file { $config_dir:
      ensure => directory,
      mode   => '0755',
    }

    file { "$config_dir/conf.d":
      ensure  => directory,
      purge   => $purge_config,
      recurse => $purge_config,
      mode    => '0775',
      notify  => Service['logstash'],
    }

    file {     "$config_dir/patterns":
      ensure  => directory,
      purge   => $purge_config,
      recurse => $purge_config,
      mode    => '0755',
    }
  }
  elsif($ensure == 'absent') {
    # Completely remove the config directory. ie. 'rm -rf /etc/logstash'
    file { $config_dir:
      ensure  => 'absent',
      recurse => true,
      force   => true,
    }
  }
}
